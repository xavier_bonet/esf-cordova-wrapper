# Cordova wrapper pel joc ESF

## Definició
Aplicació Apache Cordova 3.1.0 amb els plugins [InAppBrowser](http://cordova.apache.org/docs/en/3.1.0/cordova_inappbrowser_inappbrowser.md.html#InAppBrowser) i [Splashscreen](http://cordova.apache.org/docs/en/3.1.0/cordova_splashscreen_splashscreen.md.html#Splashscreen) activats. Plataforma Android de moment.

## Estructura
+ `merges` serveix per a aplicar customitzacions per a cada plataforma, per exemple per tenir un codi o assets diferents a Android que a iOS. No ho fem servir.

+ `platforms`
	+ `android` projecte Android nadiu que es pot afegir a Eclipse.
		- `AndroidManifest.xml` permisos, versió SDK, orientació de l'app.
		- `bin` aquí es generen els `Elpreudelabundncia-debug.apk` amb la comanda `cordova build`. Per a generar els apk de producció signats es recomana fer-ho des d'Eclipse com si fos un projecte Android nadiu.
		- `src/es/isf/esfab/ElPreuDelAbundancia.java` Activitat Android principal de l'aplicació. Aquí es defineix el timeout màxim de l'splash screen.
		- `res` conté la icona i splash screen d'aplicació per a diverses densitats de pantalla. Si es vol canviar algun dels fitxers simplement cal mirar que la nova versió tingui la mateixa resolució.

+ `plugins` conté els plugins mencionats anteriorment. Per a afegir o eliminar plugins **cal fer servir l'eina de consola de cordova**, no fer-ho manualment. **Llegiu la documentació dels enllaços anteriors**.

+ `www`
	- `config.xml` defineix paràmetres importants de l'aplicació generada, com el títol.
	- `index.html` versió lleugerament modificada de la pàgina principal del joc. Si mai es fa cap canvi a l'index del projecte principal, caldrà propagar-lo aquí.
	- `src` aquí dintre hi hauria el codi de l'aplicació web. En el meu cas faig un enllaç simbòlic a la carpeta `src` del projecte principal.

## Splash screen amb format 9-patch

Recomano llegir primer [aquesta excel·lent explicació](http://radleymarx.com/blog/simple-guide-to-9-patch/) sobre el format.

Bàsicament es tracta de crear primer un PNG amb la mida que demana Android per a la densitat de pantalla objectiu, i després pintar unes línies negres (`#000000`) de 1px d'ample als 4 costats per a definir quina part és contingut que no es pot escalar i quina sí.

La banda inferior i dreta defineixen l'amplada i alçada del contingut. En el nostre cas abarcaran tot el text que conforma el títol, que no es pot escalar perquè quedaria distorsionat.

La banda superior i esquerra defineix les zones escalables, que Android estirarà. En el nostre cas són unes franges a sobre, a baix i a cada banda del text.

![image](http://softwaremeetsdesign.com/Coses/9patch.png)

Per a editar les zones el més pràctic és fer servir l'eina que ve amb l'SDK Android, disponible a `Android_IDE/sdk/tools/draw9patch`. També hi ha una nova eina online però no l'he provada, i he llegit que pot donar problemes amb imatges d'alta resolució.
